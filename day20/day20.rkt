;;;
;;; --- Day 20: Particle Swarm ---
;;;
;;; Tetsumi <tetsumi@vmail.me>
;;;

#lang racket

(define particles (map (compose list->vector
                                (curry filter-map string->number)
                                (curryr string-split #px"<|>|,"))
                       (port->lines)))

(define (simulate p step)
  (define accelX (vector-ref p 6))
  (define accelY (vector-ref p 7))
  (define accelZ (vector-ref p 8))

  (do ([velX (vector-ref p 3) (+ velX accelX)]
       [velY (vector-ref p 4) (+ velY accelY)]
       [velZ (vector-ref p 5) (+ velZ accelZ)]
       [posX (vector-ref p 0) (+ posX velX)]
       [posY (vector-ref p 1) (+ posY velY)]
       [posZ (vector-ref p 2) (+ posZ velZ)]
       [i 0 (add1 i)])
      ((> i step) (+ (abs posX) (abs posY) (abs posZ)))))

(displayln (let ([l (map (curryr simulate 1000) particles)])
             (index-of l (argmin identity l))))

(define (update! p)
  (define velX (+ (vector-ref p 3) (vector-ref p 6)))
  (define velY (+ (vector-ref p 4) (vector-ref p 7)))
  (define velZ (+ (vector-ref p 5) (vector-ref p 8)))
  (vector-set! p 3 velX)
  (vector-set! p 4 velY)
  (vector-set! p 5 velZ)
  (let ([nX (+ (vector-ref p 0) velX)]
        [nY (+ (vector-ref p 1) velY)]
        [nZ (+ (vector-ref p 2) velZ)])
    (vector-set! p 0 nX)
    (vector-set! p 1 nY)
    (vector-set! p 2 nZ))
  p)

(displayln
 (length (for/fold ([l particles])
                   ([i (in-range 1000)])
           (let loop ([nl l])
             (let ([dup (check-duplicates nl #:key (curryr vector-take 3))])
               (if dup
                   (loop (remove* (cons dup '())
                                  nl
                                  (lambda (a b)
                                    (and
                                     (= (vector-ref a 0) (vector-ref b 0))
                                     (= (vector-ref a 1) (vector-ref b 1))
                                     (= (vector-ref a 2) (vector-ref b 2))))))
                   (map update! nl)))))))
