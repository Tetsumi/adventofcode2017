;;;
;;; --- Day 21: Fractal Art ---
;;;
;;; Tetsumi <tetsumi@vmail.me>
;;;

#lang racket

(define it (make-hash))
(define toVec (compose list->vector (curry filter-not (curry char=? #\/))))

(define (rot v)
  (define len (vector-length v))
  (for/vector #:length len
      ([i (if (= len 4)
              '(1 3 0 2)
              '(2 5 8 1 4 7 0 3 6))])
    (vector-ref v i)))

(define (flip v)
  (define len (vector-length v))
  (for/vector #:length len
      ([i (if (= len 4)
              '(1 0 3 2)
              '(2 1 0 5 4 3 8 7 6))])
    (vector-ref v i)))

(for ([line (port->lines)])
  (let* ([sp (string-split line " => ")]
         [a (toVec (string->list (car sp)))]
         [b (toVec (string->list (cadr sp)))])
    (for/fold ([la a])
              ([i (in-range 3)])
      (hash-set! it la b)
      (hash-set! it (flip la) b)
      (rot la))))

(define (subvec v x y ls s)
  (for*/vector #:length (* ls ls)
      ([y (in-range y (+ y ls))]
       [x (in-range x (+ x ls))])
    (vector-ref v (+ (* y s) x))))

(define (expand v)
  (define len (vector-length v))
  (define line (sqrt len))
  (define step (or (and (= 0 (remainder len 2)) 2) 3))
  (define tstep (add1 step))
  (define amount (quotient line step))
  (define tline (* amount tstep))
  (define nextv (make-vector (sqr tline)))
  (define-syntax-rule (2D->1D x y) (+ (* y tline) x))
  (for ([ys (in-range 0 line step)]
        [yt (in-range 0 tline tstep)]
        #:when true
        [xs (in-range 0 line step)]
        [xt (in-range 0 tline tstep)])
    (let ([sub (hash-ref it (subvec v xs ys step line))])
      (for* ([y (in-range tstep)]
             [x (in-range tstep)])
        (vector-set! nextv
                     (+ (* (+ yt y) tline) xt x)
                     (vector-ref sub (+ (* y tstep) x))))))
  nextv)
    

(define countd (curry vector-count (curry char=? #\#)))

(displayln (countd (for/fold ([v #(#\. #\# #\. #\. #\. #\# #\# #\# #\#)])
                             ([i (in-range 18)])
                     (when (= i 5)
                       (displayln (countd v)))           
                     (expand v))))


