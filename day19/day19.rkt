;;;
;;; --- Day 19: A Series of Tubes ---
;;;
;;; Tetsumi <tetsumi@vmail.me>
;;;

#lang racket

(define cells (list->vector (port->lines)))

(define-syntax-rule (cell xy) (string-ref (vector-ref cells (imag-part xy))
                                          (real-part xy)))

(define height (vector-length cells))
(define width (string-length (vector-ref cells 0)))
(define xs (for/sum ([i (vector-ref cells 0)] #:break (char=? i #\|)) 1))

(define (cellBs xy)
  (let ([x (real-part xy)]
        [y (imag-part xy)])
    (or (and (>= x 0) (< x width) (>= y 0) (< y height)
             (string-ref (vector-ref cells y) x))
        #\space)))

(let-values
    ([(lc steps)
      (let loop ([pos (make-rectangular xs 0)]
                 [dir 0+1i]
                 [letters '()]
                 [steps 0])
        (let ([ch (cell pos)])
          (case ch
            [(#\| #\-) (loop (+ pos dir) dir letters (add1 steps))]
            [(#\+) (let ([off (for/first ([p '(1 -1 0+1i 0-1i)]
                                          #:unless (= p (- dir))
                                          #:when (let ([nc (cellBs (+ pos p))])
                                                   (or (char=? nc #\-)
                                                       (char=? nc #\|))))
                                p)])
                     (if off
                         (loop (+ pos off) off letters (add1 steps))
                         (values letters steps)))]
           [(#\space) (values letters steps)]
           [else (loop (+ pos dir) dir (cons ch letters) (add1 steps))])))])
  (printf "~a~%~a~%" (list->string (reverse lc)) steps))
  
